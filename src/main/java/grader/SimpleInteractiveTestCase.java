package grader;

import java.io.IOException;
import java.util.List;

public class SimpleInteractiveTestCase extends TestCase {
    ProgramRunner programRunner;
    List<String> args;
    String initialExpectedOutput;

    public static class ExpectedIO {
        String input;
        String expectedOutput;
        public ExpectedIO(String input, String expectedOutput) {
            this.input = input;
            this.expectedOutput = expectedOutput;
        }
    }

    List<ExpectedIO> expectedIO;

    public SimpleInteractiveTestCase(String name, int points, ProgramRunner programRunner, List<String> args,
                                     String initialExpectedOutput, List<ExpectedIO> expectedIO) {
        this.name = name;
        this.points = points;
        this.programRunner = programRunner;
        this.args = args;
        this.initialExpectedOutput = initialExpectedOutput;
        this.expectedIO = expectedIO;
    }

    @Override
    public Result run() {
        String output;
        try {
            output = programRunner.launchInteractive(this.args);
            output = output.trim();
        }
        catch (IOException e) {
            return new Result(false, e.getMessage());
        }

        if (!initialExpectedOutput.equals(output)) {
            String failHint = "Output Mismatch:\n---Expected Output---\n"
                    + initialExpectedOutput + "\n---Your Output---\n"
                    + output;
            return new Result(false, failHint);
        }

        StringBuilder inputSoFar = new StringBuilder();
        for (ExpectedIO expectedIO : this.expectedIO) {
            inputSoFar.append(expectedIO.input);
            try {
                output = programRunner.interact(expectedIO.input);
                output = output.trim();
            } catch (ProgramRunner.ProgramTerminatedException e) {
                return new Result(false, "Program terminated unexpectedly");
            }
            if (!expectedIO.expectedOutput.equals(output)) {
                String failHint = "Input So Far: " + inputSoFar.toString()
                        + "\n---Expected Output---\n" + expectedIO.expectedOutput
                        + "\n---Your Output---\n" + output;
                return new Result(false, failHint);
            }

            inputSoFar.append(", ");
        }
        return new Result(true, "");
    }
}
