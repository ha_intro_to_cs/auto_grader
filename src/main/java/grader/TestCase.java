package grader;

import java.util.ArrayList;
import java.util.List;

public abstract class TestCase {
    public static class Result {
        public boolean pass;
        public String hint;

        public Result(boolean pass, String hint) {
            this.pass = pass;
            this.hint = hint;
        }
    }

    protected String name;
    protected int points;
    protected List<TestCase> children = new ArrayList<>();

    public abstract Result run();

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void addTest(TestCase test) {
        children.add(test);
    }

    public List<TestCase> getChildren() {
        return children;
    }
}
