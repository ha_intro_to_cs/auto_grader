package grader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestRunner {
    private static final Logger logger = LogManager.getLogger(TestRunner.class);

    File compileDirectory;
    List<String> compileCommand;
    List<TestCase> testCases = new ArrayList<>();

    public TestRunner(File compileDirectory, List<String> compileCommand) {
        this.compileDirectory = compileDirectory;
        this.compileCommand = compileCommand;
    }

    public void addTest(TestCase test) {
        testCases.add(test);
    }

    private static class Score {
        public int points;
        public int possiblePoints;
    }

    public void runTests() {
        logger.info("Compiling in " + compileDirectory.getAbsolutePath());
        StringBuilder compileCommandString = new StringBuilder();
        compileCommandString.append("    ");
        for (String arg : compileCommand) {
            compileCommandString.append(arg).append(" ");
        }
        logger.info(compileCommandString.toString());

        Compiler.Result compileResult = new Compiler.Result();
        try {
            compileResult = Compiler.compile(compileDirectory, compileCommand);
        } catch (IOException e) {
            compileResult.success = false;
            compileResult.output = e.getMessage();
        }

        if (!compileResult.success) {
            logger.fatal("Failed to compile: " + compileResult.output);
            return;
        }

        logger.info("Successfully compiled");

        logger.info("Running tests");
        Score totalScore = new Score();
        totalScore.points = 0;
        totalScore.possiblePoints = 0;
        for (TestCase test : testCases) {
            Score testScore = testHelper(test, 1);
            totalScore.points += testScore.points;
            totalScore.possiblePoints += testScore.possiblePoints;
        }

        logger.info("Total Score: " + totalScore.points + "/" + totalScore.possiblePoints);
    }

    private Score testHelper(TestCase test, int level) {
        if (test.getChildren().size() == 0) {
            String testResult = getIndentationPadding(level) + padRight(test.getName() + ": ", 20, ' ');
            Score score = new Score();
            score.possiblePoints = test.getPoints();
            score.points = 0;
            TestCase.Result result = test.run();
            String resultText;
            if (result.pass) {
                score.points = score.possiblePoints;
                resultText = "PASS";
            }
            else {
                resultText = "FAIL";
            }

            testResult += resultText + " (" + score.possiblePoints + ")";
            logger.info(testResult);

            if (!result.pass) {
                // The hint could contain multiple lines, doing it this way means that everything stays at a nice
                // indentation level
                for (String hintLine : result.hint.split("\\R")) {
                    logger.debug(getIndentationPadding(level + 1) + hintLine);
                }
            }

            return score;
        }

        logger.info(getIndentationPadding(level) + test.getName());
        Score score = new Score();
        score.possiblePoints = 0;
        score.points = 0;
        for (TestCase subTest : test.getChildren()) {
            Score subScore = testHelper(subTest, level + 1);
            score.points += subScore.points;
            score.possiblePoints += subScore.possiblePoints;
        }

        logger.info(getIndentationPadding(level) + padRight(test.getName() + " Total: ", 24, ' ') + score.points + "/" + score.possiblePoints);
        return score;
    }

    private String getIndentationPadding(int level) {
        StringBuilder padding = new StringBuilder();
        for (int i = 0; i < level; i++) {
            padding.append("    ");
        }
        return padding.toString();
    }

    private String padRight(String text, int length, char fill) {
        StringBuilder padding = new StringBuilder();
        while (padding.length() < length - text.length()) {
            padding.append(fill);
        }
        return text + padding.toString();
    }
}
