package grader;

import java.io.IOException;
import java.util.List;

public class SimpleTestCase extends TestCase {
    ProgramRunner programRunner;
    List<String> args;
    String expectedOutput;

    public SimpleTestCase(String name, int points, ProgramRunner programRunner, List<String> args,
                          String expectedOutput) {
        this.name = name;
        this.points = points;
        this.programRunner = programRunner;
        this.args = args;
        this.expectedOutput = expectedOutput;
    }

    @Override
    public Result run() {
        String output;
        try {
            output = programRunner.launchNoInteract(args);
            output = output.trim();
        }
        catch (IOException e) {
            return new Result(false, e.getMessage());
        }

        String failHint = "Output mismatch:\n---Expected Output---\n"
                + expectedOutput + "\n---Your Output---\n" + output;

        return new Result(expectedOutput.equals(output), failHint);
    }
}
