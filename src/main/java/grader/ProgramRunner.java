package grader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ProgramRunner {
    private File workingDirectory;
    private List<String> runCommand;
    private Process process;

    public ProgramRunner(File workingDirectory, List<String> runCommand) {
        this.workingDirectory = workingDirectory;
        this.runCommand = runCommand;
    }

    /**
     * Launches the program with the expectation that it will terminate with no further input.
     *
     * @return
     * @throws IOException
     */
    public String launchNoInteract(List<String> args) throws IOException {
        terminate();
        ProcessBuilder processBuilder = new ProcessBuilder();
        List<String> command = new ArrayList<>(runCommand);
        if (args != null) {
            command.addAll(args);
        }
        processBuilder.command(command);
        processBuilder.directory(workingDirectory);
        process = processBuilder.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }

    public String launchNoInteract() throws IOException {
        return launchNoInteract(null);
    }

    public String launchInteractive(List<String> args) throws IOException {
        terminate();
        ProcessBuilder processBuilder = new ProcessBuilder();
        List<String> command = new ArrayList<>(runCommand);
        if (args != null) {
            command.addAll(args);
        }
        processBuilder.command(command);
        processBuilder.directory(workingDirectory);
        process = processBuilder.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        int characterCode;
        char prevCharacter = 'z';
        boolean promptBreak = false;
        while ((characterCode = reader.read()) != -1) {
            char character = (char) characterCode;
            builder.append(character);
            // funny, this is exactly the kind of bug I tell my students not to write...
            if (prevCharacter == '>' && character == ' ') {
                promptBreak = true;
                break;
            }
            prevCharacter = character;
        }
        if (promptBreak) {
            // remove the "> " characters
            builder.setLength(Math.max(builder.length() - 2, 0));
        }

        return builder.toString();
    }

    public String launchInteractive() throws IOException {
        return launchInteractive(null);
    }

    public static class ProgramTerminatedException extends Exception {
        ProgramTerminatedException(String s){
            super(s);
        }
    }

    public String interact(String input) throws ProgramTerminatedException {
        BufferedWriter writer =
                new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
        try {
            writer.write(input);
            writer.newLine();
            writer.flush();
        }
        catch (IOException e) {
            throw new ProgramTerminatedException(e.getMessage());
        }

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        int characterCode;
        char prevCharacter = 'z';
        boolean promptBreak = false;
        try {
            while ((characterCode = reader.read()) != -1) {
                char character = (char) characterCode;
                builder.append(character);
                // funny, this is exactly the kind of bug I tell my students not to write...
                if (prevCharacter == '>' && character == ' ') {
                    promptBreak = true;
                    break;
                }
                prevCharacter = character;
            }
            if (promptBreak) {
                // remove the "> " characters
                builder.setLength(Math.max(builder.length() - 2, 0));
            }
        }
        catch (IOException e) {
            throw new ProgramTerminatedException(e.getMessage());
        }

        return builder.toString();
    }

    public void terminate() {
        if (process != null) {
            process.destroy();
        }
    }
}
