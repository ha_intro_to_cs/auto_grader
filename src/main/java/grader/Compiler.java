package grader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Compiler {
    public static class Result {
        public boolean success;
        public String output;
    }

    public static Result compile(File workingDirectory, List<String> compileCommand) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(compileCommand);
        processBuilder.directory(workingDirectory);
        Process process = processBuilder.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.lineSeparator());
        }
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Result result = new Result();
        result.success = process.exitValue() == 0;
        result.output = builder.toString().trim();

        return result;
    }
}
