import grader.ProgramRunner;
import grader.SimpleTestCase;
import grader.TestCategory;
import grader.TestRunner;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Example {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        List<String> compileCommand = Arrays.asList("echo");
        TestRunner runner = new TestRunner(Paths.get("/").toFile(), compileCommand);

        List<String> runCommand = Arrays.asList("echo");
        ProgramRunner programRunner = new ProgramRunner(Paths.get("/").toFile(), runCommand);

        List<String> oneCommand = Arrays.asList("one");
        SimpleTestCase one = new SimpleTestCase("One", 10, programRunner, oneCommand, "one");

        List<String> twoCommand = Arrays.asList("two");
        SimpleTestCase two = new SimpleTestCase("Two", 10, programRunner, twoCommand, "two");

        List<String> threeCommand = Arrays.asList("three");
        SimpleTestCase three = new SimpleTestCase("Three", 10, programRunner, threeCommand, "three");

        TestCategory testCat = new TestCategory("Test Cat");
        testCat.addTest(one);
        testCat.addTest(two);
        testCat.addTest(three);

        List<String> multiCommand = Arrays.asList("code", "lyoko");
        SimpleTestCase multi = new SimpleTestCase("Multi", 10, programRunner, multiCommand, "code lyok");

        TestCategory complexCat = new TestCategory("Complex");
        complexCat.addTest(multi);

        runner.addTest(testCat);
        runner.addTest(complexCat);

        runner.runTests();
    }
}
